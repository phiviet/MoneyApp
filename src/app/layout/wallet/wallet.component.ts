import { Component, OnInit } from '@angular/core';
import { IWallet } from '../../model/wallet.model';
import { Wallet } from '../../service/wallet.service';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.scss']
})
export class WalletComponent implements OnInit {
  dataWallets: IWallet[] = [];
  constructor(private Wallet: Wallet) { }

  ngOnInit() {
    this.getDataWallets();
    // console.log(this.dataWallets);
  }

  getDataWallets() {
    this.Wallet.getWallets()
    .subscribe(
      result => this.dataWallets = result,
      error=> console.log("Error::  " + error)
    )
    console.log(this.dataWallets);
  }
}
