import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavbarComponent } from './layout/navbar/navbar.component';
import { OverviewComponent } from './layout/overview/overview.component';
import { SavingsComponent } from './layout/savings/savings.component';
import { AppRoutingModule } from './app-routing.module';
import { LayoutComponent } from './layout/layout.component';
import { LoginComponent } from './login/login.component';
import { WalletComponent } from './layout/wallet/wallet.component';

import { HttpModule } from '@angular/http';
import { Wallet } from './service/wallet.service';
import { AddwalletComponent } from './layout/wallet/addwallet/addwallet.component';
import { TravelComponent } from './layout/travel/travel.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    OverviewComponent,
    SavingsComponent,
    LayoutComponent,
    LoginComponent,
    WalletComponent,
    AddwalletComponent,
    TravelComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule
  ],
  providers: [Wallet],
  bootstrap: [AppComponent]
})
export class AppModule { }
