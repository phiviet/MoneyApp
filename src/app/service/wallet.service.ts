import { IWallet } from './../model/wallet.model';
import { Injectable } from '@angular/core';
import { Http, Response} from '@angular/http';
// import {HttpClient} from '@angular/common/http';

// import 'rxjs/add/operator/map';
import 'rxjs/Rx';

import { Observable } from 'rxjs/Observable';

@Injectable()
export class Wallet{
    
    constructor(private http:Http){}
    // getDataWallets(): Promise<any>{
    //     return this.Http.get('http://localhost:8081/wallet')
    //     .toPromise()
    //     .then(response => response.json())
    //     .catch(err => err);
    // }

    getWallets():Observable<IWallet[]>{
        return this.http
        .get('http://localhost:8081/wallet')
        .map((response: Response) => {
            return <IWallet[]>response.json();
        })
        .catch(this.handleError);
    }
    private handleError(error: Response){
        return Observable.throw(error.statusText);
    }
}   