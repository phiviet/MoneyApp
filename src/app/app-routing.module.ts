import { TravelComponent } from './layout/travel/travel.component';
import { LayoutComponent } from './layout/layout.component';
import { SavingsComponent } from './layout/savings/savings.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { OverviewComponent } from './layout/overview/overview.component';

const newLocal = 'path';

const routes: Routes = [
  // { path: '', pathMatch: 'full', redirectTo: '' },
  { path: '', component: LayoutComponent, children: [
    {path: '', pathMatch: 'full', redirectTo: 'wallets'},
    { path: 'wallets', component: OverviewComponent },
    { path: 'savings', component: SavingsComponent},
    { path: 'travels', component: TravelComponent },
  ] },
  // { path: 'wallet', pathMatch: 'full', component: LayoutComponent, children: [
  //     { path: '', component: OverviewComponent },
  //     { path: 'saving', component: SavingsComponent}
  // ] },
  { path: 'login', component: LoginComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
